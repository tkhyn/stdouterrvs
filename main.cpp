#include "mainwindow.h"

#include <thread>
#include <vector>

#include <QApplication>
#include <QDebug>

#include "StdOutAndErrToVSOutput.h"


void t(int n)
{
    std::cout << "Hello World from thread " << std::to_string(n) << std::endl;
}


int main(int argc, char *argv[])
{
    VSTDOUT

    QApplication a(argc, argv);

    std::vector<std::thread> threads;
    threads.reserve(10);
    for (int i = 0; i < 10; i++) {
        threads.emplace_back(t, i);
    }

    for (auto& t : threads) {
        t.join();
    }

    MainWindow w;
    w.show();

    qDebug() << "Hello World on qDebug";
    std::cout << "Hello World on cout" << std::endl;
    std::cerr << "Hello World on cerr" << std::endl;

    return QApplication::exec();
}

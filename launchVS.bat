:: use this batch file to launch Visual Studio 2017 and be able to run / debug from the IDE

:: <rant>The geniuses at Microsoft thought it was not a good idea to be able to add a path to the
:: PATH environment variable when running / debugging a compiled executable. So we have to resort to
:: this hack to avoid polluting the global PATH...</rant>
:: see https://developercommunity.visualstudio.com/content/problem/131643/how-to-set-a-cmake-path-for-dlls.html

@echo off

:: add Qt dll path to PATH
set PATH=C:\Qt\5.12.1\msvc2017_64\bin;%PATH%

:: launch VS2017
echo Launching Visual Studio 2017
@start "" "C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\Common7\IDE\devenv.exe" %~dp0\..\ source:JumpList

echo All done. Exiting

exit

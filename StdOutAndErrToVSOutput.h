#pragma once

#include <iostream>

#ifdef VISUAL_STUDIO

#define VSTDOUT StdOutAndErrToVSOutput stdOutAndErrToVSOutput;

#include <Windows.h>

class DStreamBuf : public std::streambuf {
public:
    int_type overflow(int_type c) override {
        if (c != EOF) {
            TCHAR buf[] = { static_cast<TCHAR>(c), '\0' };
            OutputDebugString(buf);
        }
        return c;
    }
};

class StdOutAndErrToVSOutput {

    DStreamBuf dStreamBuf;
    std::streambuf *out_stream;
    std::streambuf *err_stream;

public:
    StdOutAndErrToVSOutput() {
        out_stream = std::cout.rdbuf(&dStreamBuf);
        err_stream = std::cerr.rdbuf(&dStreamBuf);
    }

    ~StdOutAndErrToVSOutput() {
        std::cout.rdbuf(out_stream);
        std::cerr.rdbuf(err_stream);
    }
};

#else

#define VSTDOUT

#endif // VISUAL_STUDIO
